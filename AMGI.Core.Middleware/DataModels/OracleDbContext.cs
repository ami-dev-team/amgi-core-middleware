﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace AMGI.Core.Middleware.DataModels
{
    public class OracleDbContext
    {
        private string CONN_STRING = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.20.122.110)(PORT=1525))(CONNECT_DATA=(SERVICE_NAME=SICL))); User Id=SICL; Password=GoGoLive785";
        //private string CONN_STRING = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.20.189.110)(PORT=1525))(CONNECT_DATA=(SERVICE_NAME=SICL))); User Id=SICL; Password=GoGoLive785";

        private OracleConnection connection = null;

        public OracleDbContext()
        {
            connection = new OracleConnection(CONN_STRING);
            CheckConnectionState();
        }

        public DataTable GetPaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_POLICY_DATA_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetFirePaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_FI_PREMIUM_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetMotorPaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_MT_PREMIUM_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetMotorFleetPaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_MT_FLEET_PREMIUM_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetMarinePaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_MR_PREMIUM_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetMTransitPaymentData(string proposal)
        {
            try
            {
                DataTable TempData = new DataTable();

                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_MIT_PREMIUM_BY_PROPOSAL_NO WHERE PROPOSAL_NO ='" + proposal + "'";

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }
                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void CheckConnectionState()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region claim
        public DataTable GetClaimData()
        {
            try
            {
                DataTable TempData = new DataTable();
                
                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                //OraCommand2.CommandText = "select INT_CLAIM_NO from cl_t_intimation where INT_STATUS = 'O' and INT_CLAIM_NO = 'CL/AMI/MCC/17000001'";
                OraCommand2.CommandText = "select ClaimNo from VW_GETCLAIM_DATA";
                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }

                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetClaimDataByClaimNo(string claimno)
        {
            try
            {
                DataTable TempData = new DataTable();
                 
                OracleCommand OraCommand2 = connection.CreateCommand();
                OraCommand2.CommandType = CommandType.Text;
                OraCommand2.CommandText = "SELECT * FROM VW_GETCLAIM_DATA";
                OraCommand2.Parameters.Add(new OracleParameter("ClaimNo", OracleDbType.Varchar2));
                OraCommand2.Parameters[0].Value = claimno;

                CheckConnectionState();
                OracleDataReader reader = OraCommand2.ExecuteReader();

                if (reader.HasRows)
                {
                    TempData.Load(reader);
                }
                else
                {
                    throw new Exception("No data found!");
                }

                return TempData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion
    }
}