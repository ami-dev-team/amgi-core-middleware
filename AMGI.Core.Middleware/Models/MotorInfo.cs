﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.Core.Middleware.Models
{
    public class MotorInfo
    {
        public string PROPOSAL_NO { get; set; }
        public decimal TOTAL_PREMIUM { get; set; }
        public string INSURED_NAME { get; set; }
        public string VEHICLE_NO { get; set; }
        public DateTime PERIOD_FROM { get; set; }
        public DateTime PERIOD_TO { get; set; }
    }
}
