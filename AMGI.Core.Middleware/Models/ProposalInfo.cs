﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.Core.Middleware.Models
{
    public class ProposalInfo
    {
        public string CLA_CODE { get; set; }
        public string PRD_CODE { get; set; }
        public string PROPOSAL_NO { get; set; }
        public string POLICY_NO { get; set; }
        public string TRANS_TYPE { get; set; }
        public string SUM_INSURED { get; set; }
        public string CUS_CODE { get; set; }
        public string CUS_NAME { get; set; }
        public string CUS_ADDRESS { get; set; }
        public string CURRENCY { get; set; }
        public decimal TRANS_AMT { get; set; }
        public decimal TOTAL_PREMIUM { get; set; }
        public decimal TOTAL_TRANS_AMT { get; set; }
        public string EXECUTIVE_CODE { get; set; }
        public string AGENT_NAME { get; set; }
        public string ACC_HANDLER_NAME { get; set; }
        public string AGENT_CODE { get; set; }
        public decimal? COMMISSION_AMT { get; set; }
        public string DEBIT_NOTE_NO { get; set; }
        public string TEM_DEBIT_NOTE_NO { get; set; }
    }
}
