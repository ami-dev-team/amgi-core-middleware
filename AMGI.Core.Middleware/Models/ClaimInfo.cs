﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMGI.Core.Middleware.Models
{
    [Serializable]
    public class ClaimInfo
    {
        public DateTime ACCIDENT_DATE { get; set; }

        //public TimeSpan ACCIDENT_TIME { get; set; }

        public string CUSTOMER_VEHICLENO { get; set; }

        public string ClaimNo { get; set; }

        public DateTime REPORT_DATE { get; set; }

        public string CAUSE_OF_LOSS { get; set; }

        public string ACCIDENT_PLACE { get; set; }

        public string DRIVER_NAME { get; set; }

        public string EXTENT_OF_DAMAGE { get; set; }

        public string REMARK { get; set; }

        public string ACCIDENT_DESCRIPTION { get; set; }

        public string REPORTER_NAME { get; set; }

        public string CONTACT { get; set; }

        public string CLAIM_CATEGORY { get; set; }

        public string CUSTOMER_NAME { get; set; }

        public string SURVEYOR_NAME { get; set; }

        public string POLICY_NO { get; set; }
    }
}
