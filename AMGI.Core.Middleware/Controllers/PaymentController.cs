﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMGI.Core.Middleware.DataModels;
using AMGI.Core.Middleware.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AMGI.Core.Middleware.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class PaymentController : Controller
    {
        OracleDbContext context = null;
        DataTable tempData = null;

        public PaymentController()
        {
            context = new OracleDbContext();
        }

        [HttpGet("{id}", Name = "Get")]
        public ActionResult Get(string id)
        {
            //Convert base64 encoded string to regular string.
            byte[] decode_id = Convert.FromBase64String(id);
            id = Encoding.UTF8.GetString(decode_id);

            string pcode = id.Substring(id.ToString().Length - 12, 3);

            try
            {
                if (pcode == "FCS" || pcode == "FCT" || pcode == "FFG" || pcode == "FFI" || pcode == "FSD")
                {
                    //Populate data to temp data table.
                    tempData = context.GetFirePaymentData(id);

                    if (tempData == null)
                    {
                        return NotFound();
                    }

                    string jsonPolicy = JsonConvert.SerializeObject(tempData);
                    //Modified by Htet Paing 'coz of report format changed!
                    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
                    IEnumerable<FireInfo> fire = JsonConvert.DeserializeObject<IEnumerable<FireInfo>>(jsonPolicy);

                    if (fire == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        var f = fire.SingleOrDefault(x => x.PROPOSAL_NO == id);
                        if (f == null)
                        {
                            return NotFound();
                        }
                        else
                        {
                            return new JsonResult(f);
                        }
                    }
                }

                else if (pcode == "MCC" || pcode == "MCP" || pcode == "MTC")
                {
                    tempData = context.GetMotorPaymentData(id);

                    if (tempData == null)
                    {
                        return NotFound();
                    }

                    string jsonPolicy = JsonConvert.SerializeObject(tempData);
                    //Modified by Htet Paing 'coz of report format changed!
                    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
                    IEnumerable<MotorInfo> motor = JsonConvert.DeserializeObject<IEnumerable<MotorInfo>>(jsonPolicy);

                    if (motor == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        var m = motor.SingleOrDefault(x => x.PROPOSAL_NO == id);
                        if (m == null)
                        {
                            return NotFound();
                        }
                        else
                        {
                            return new JsonResult(m);
                        }
                    }
                }

                else if (pcode == "MFC" || pcode == "MFP" || pcode == "MCH")
                {
                    tempData = context.GetMotorFleetPaymentData(id);

                    if (tempData == null)
                    {
                        return NotFound();
                    }

                    string jsonPolicy = JsonConvert.SerializeObject(tempData);
                    //Modified by Htet Paing 'coz of report format changed!
                    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
                    IEnumerable<MotorInfo> motor = JsonConvert.DeserializeObject<IEnumerable<MotorInfo>>(jsonPolicy);

                    if (motor == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        var m = motor.SingleOrDefault(x => x.PROPOSAL_NO == id);
                        if (m == null)
                        {
                            return NotFound();
                        }
                        else
                        {
                            return new JsonResult(m);
                        }
                    }
                }

                else if (pcode == "MIT")
                {
                    tempData = context.GetMTransitPaymentData(id);

                    if (tempData == null)
                    {
                        return NotFound();
                    }

                    string jsonPolicy = JsonConvert.SerializeObject(tempData);
                    //Modified by Htet Paing 'coz of report format changed!
                    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
                    IEnumerable<MarineTransit> marinetransit = JsonConvert.DeserializeObject<IEnumerable<MarineTransit>>(jsonPolicy);

                    if (marinetransit == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        var mt = marinetransit.SingleOrDefault(x => x.PROPOSAL_NO == id);
                        if (mt == null)
                        {
                            return NotFound();
                        }
                        else
                        {
                            return new JsonResult(mt);
                        }
                    }
                }

                else
                {
                    tempData = context.GetMarinePaymentData(id);

                    if (tempData == null)
                    {
                        return NotFound();
                    }

                    string jsonPolicy = JsonConvert.SerializeObject(tempData);
                    //Modified by Htet Paing 'coz of report format changed!
                    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
                    IEnumerable<MarineInfo> marine = JsonConvert.DeserializeObject<IEnumerable<MarineInfo>>(jsonPolicy);

                    if (marine == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        var mr = marine.SingleOrDefault(x => x.PROPOSAL_NO == id);
                        if (mr == null)
                        {
                            return NotFound();
                        }
                        else
                        {
                            return new JsonResult(mr);
                        }
                    }
                }
            }
            catch(Exception)
            {
                return NotFound();
            }
        }

        #region ++ Comments ++

        //[HttpGet("{id}", Name = "Fire")]
        //public ActionResult Fire(string id)
        //{
        //    //Convert base64 encoded string to regular string.
        //    byte[] decode_id = Convert.FromBase64String(id);
        //    id = Encoding.UTF8.GetString(decode_id);

        //    //Populate data to temp data table.
        //    tempData = context.GetFirePaymentData(id);

        //    if (tempData == null)
        //    {
        //        return NotFound();
        //    }

        //    string jsonPolicy = JsonConvert.SerializeObject(tempData);
        //    //Modified by Htet Paing 'coz of report format changed!
        //    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
        //    IEnumerable<ProposalInfo> proposal = JsonConvert.DeserializeObject<IEnumerable<ProposalInfo>>(jsonPolicy);

        //    if (proposal == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        var propsal = proposal.SingleOrDefault(x => x.PROPOSAL_NO == id);
        //        if (propsal == null)
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            return new JsonResult(propsal);
        //        }
        //    }
        //}

        //[HttpGet("{id}", Name = "Motor")]
        //public ActionResult Motor(string id)
        //{
        //    //Convert base64 encoded string to regular string.
        //    byte[] decode_id = Convert.FromBase64String(id);
        //    id = Encoding.UTF8.GetString(decode_id);

        //    //Populate data to temp data table.
        //    tempData = context.GetMotorPaymentData(id);

        //    if (tempData == null)
        //    {
        //        return NotFound();
        //    }

        //    string jsonPolicy = JsonConvert.SerializeObject(tempData);
        //    //Modified by Htet Paing 'coz of report format changed!
        //    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
        //    IEnumerable<ProposalInfo> proposal = JsonConvert.DeserializeObject<IEnumerable<ProposalInfo>>(jsonPolicy);

        //    if (proposal == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        var propsal = proposal.SingleOrDefault(x => x.PROPOSAL_NO == id);
        //        if (propsal == null)
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            return new JsonResult(propsal);
        //        }
        //    }
        //}

        //[HttpGet("{id}", Name = "Marine")]
        //public ActionResult Marine(string id)
        //{
        //    //Convert base64 encoded string to regular string.
        //    byte[] decode_id = Convert.FromBase64String(id);
        //    id = Encoding.UTF8.GetString(decode_id);

        //    //Populate data to temp data table.
        //    tempData = context.GetMarinePaymentData(id);

        //    if (tempData == null)
        //    {
        //        return NotFound();
        //    }

        //    string jsonPolicy = JsonConvert.SerializeObject(tempData);
        //    //Modified by Htet Paing 'coz of report format changed!
        //    //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
        //    IEnumerable<ProposalInfo> proposal = JsonConvert.DeserializeObject<IEnumerable<ProposalInfo>>(jsonPolicy);

        //    if (proposal == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        var propsal = proposal.SingleOrDefault(x => x.PROPOSAL_NO == id);
        //        if (propsal == null)
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            return new JsonResult(propsal);
        //        }
        //    }
        //}

        #endregion
    }
}