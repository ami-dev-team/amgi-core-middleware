﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMGI.Core.Middleware.DataModels;
using AMGI.Core.Middleware.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Ami.Health.Middleware.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ClaimController : Controller
    {
        OracleDbContext context = null;
        DataTable tempData = null;

        public ClaimController()
        {
            context = new OracleDbContext();
        }
        
        // GET: policies
        [HttpGet]
        public ActionResult Get()
        { 
            //Populate data to temp data table.
            tempData = context.GetClaimData();

            if (tempData == null)
            {
                return NotFound();
            }

            string jsonPolicy = JsonConvert.SerializeObject(tempData);
            //Modified by Htet Paing 'coz of report format changed! 
            IEnumerable<ClaimInfo> ClaimInfo = JsonConvert.DeserializeObject<IEnumerable<ClaimInfo>>(jsonPolicy);

            if (ClaimInfo == null)
            {
                return NotFound();
            }
            else
            {
                return new JsonResult(ClaimInfo);
            }
        }

        // GET: policies/xxx-xxx-xxx
        [HttpGet("{claimno}", Name = "GetClaim")]
        public ActionResult Get(string claimno)
        {
            //Convert base64 encoded string to regular string.
            byte[] decode_id = Convert.FromBase64String(claimno);
            claimno = Encoding.UTF8.GetString(decode_id);

            //Populate data to temp data table.
            tempData = context.GetClaimDataByClaimNo(claimno);

            if (tempData == null)
            {
                return NotFound();
            }

            string jsonPolicy = JsonConvert.SerializeObject(tempData);
            //Modified by Htet Paing 'coz of report format changed!
            //IEnumerable<Policy> policies = JsonConvert.DeserializeObject<IEnumerable<Policy>>(jsonPolicies);
            IEnumerable<ClaimInfo> claimInfo = JsonConvert.DeserializeObject<IEnumerable<ClaimInfo>>(jsonPolicy);

            if (claimInfo == null)
            {
                return NotFound();
            }
            else
            {
                var claim = claimInfo.SingleOrDefault(x => x.ClaimNo == claimno);
                if (claim == null)
                {
                    return NotFound();
                }
                else
                {
                    return new JsonResult(claim);
                }
            }
        }
    }
}